#!/bin/bash
# redirect the line which matches with 404 in actual log file
cat samplelog | grep "HTTP/1.1\" 404" > http404

cat samplelog | grep "HTTP/1.1\" [0-9]" > allhttpresponses

# >> redirect_log 

# count the lines containing with specific patern
echo "Total Number of http errors =" `cat http404 | wc -l`
echo "Total Number of allhttp responses =" `cat allhttpresponses | wc -l`
